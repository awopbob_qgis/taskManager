import processing
from time import sleep

MESSAGE_CATEGORY = 'Demonstration of the fromFunction method'

""" the three functions used by the task """

def your_function(task, wait_time, params):
    """
    Raises an exception to abort the task.
    Returns a result if success.
    The result will be passed, together with the exception (None in
    the case of success), to the on_finished method.
    If there is an exception, there will be no result.
    """
    # print a message to the Log Messages
    QgsMessageLog.logMessage('Started task {}'.format(task.description()),
                             MESSAGE_CATEGORY, Qgis.Info)
    # initialize wait_time and iterations
    wait_time = wait_time / 100
    iterations = 0

    # this if block is useful to cancel the task in the Task Manager
    if task.isCanceled():
        stopped(task)
        return None

    # execute the processing algorithm and store as "processing_output" variable
    processing_output = processing.run('native:fieldcalculator', params)
    # use this line in the console to get informations about input and output parameters of the process
    # processing.algorithmHelp('native:fieldcalculator') 

    # the processing output is a python dictionary. 
    # python dictionaries are filled with 'key':value pairs (like JSON files). Example:
    # {'OUTPUT': layer_object}
    # to get the value layer_object, use processing_output['OUTPUT']
    # and store it in a new variable "output_layer"
    output_layer = processing_output['OUTPUT']

    # demonstration of a for loop to iterate over features
    for feature in output_layer.getFeatures():
        print(feature['id_geoport']) # only use print for debugging purposes, it slows down your code
        iterations += 1 # short for "iterations = iterations + 1"

        if iterations == 10: # for demonstration purposes, we only print 10 features
            break # break out of the loop after 10 iterations

        sleep(0.5) # you can use the variable wait_time here

        # This is the same if block as before.
        # The for loop has to finish, if this block is omitted inside of it
        if task.isCanceled():
            stopped(task)
            return None

    # create the result dictionary used for further steps
    result={'iterations': iterations,
            'task': task.description(),
            'output_layer': output_layer} 
            # this dictionnary can be extended by taking other 'key':value items
    return result


def stopped(task):
    # this function is called by the function "your_function" when the task is canceled
    # it is used to print an informative message to the Log Messages panel
    QgsMessageLog.logMessage(
            'Task "{name}" was canceled'.format(
                name=task.description()),
        MESSAGE_CATEGORY, Qgis.Info)


def completed(exception, result=None):
    # this function is called when "your_function" finishes (or has a bug)
    """This is called when your_function is finished.
    Exception is not None if your_function raises an exception.
    result is the return value of your_function."""
    if exception is None: # this code block is entered if "your_function" did not raise an exception
        if result is None: # this code block is entered when "your_function" has no result and didn't raise an exception
            # it is used to print an informative message to the Log Messages
            QgsMessageLog.logMessage(
                    'Completed with no exception and no result '\
                '(probably manually canceled by the user)',
                MESSAGE_CATEGORY, Qgis.Warning)
        else: # this is the code block when "your_function" has a result and didn't raise an exception
            # it is also used to print an informative message to the Log Messages
            QgsMessageLog.logMessage(
                    'Task {name} completed\n'
                'With {iterations} '
                'iterations)'.format(
                        name=result['task'],
                    iterations=result['iterations']),
                MESSAGE_CATEGORY, Qgis.Info)
            # here we load the layer to the mapCanvas by calling the value of the key 'output_layer' in the result dictionary
            if result['output_layer'] and result['output_layer'].isValid():
                QgsProject.instance().addMapLayer(result['output_layer'])
                print('Hehe, we did it!!!')

            
    else:
        QgsMessageLog.logMessage("Exception: {}".format(exception),
                                 MESSAGE_CATEGORY, Qgis.Critical)
        raise exception

""" the algorithm block """

# Get the input layer from the mapCanvas
layers = iface.mapCanvas().layers() # returns a list of the layers shown in the mapCanvas
for layer in layers: # iterate over the layers list
    if(layer.name() == '3. fromFunction QgsTask'): # if the layer iterated equals the string
        input_layer = layer # pass the layer to the input_layer variable
        break # and break out of the loop

# Define the parameters for the processing algorithm
params = {
    'INPUT' : input_layer,
    'FIELD_NAME': 'adr_label',
    'FIELD_TYPE': 2, # 2 == String
    'FIELD_LENGTH': 254,
    'FIELD_PRECISION': 0, # only used for field types with decimal values
    'FORMULA': "if(length(numero) = 0, '', numero || ', ') || rue || '\nL-' || code_posta || ' ' || localite",
    'OUTPUT': 'memory:{}'.format(input_layer.name() + '_output') # here we use the output in memory, could also write to disk
}
# to get the documentation on processing algorithm inputs and outputs, use the following line:
# processing.algorithmHelp('native:fieldcalculator') 

# Create a task with the method fromFunction()
task = QgsTask.fromFunction('Fieldcalculator using QgsTask fromFunction', your_function,
                             on_finished=completed, wait_time=4, params=params)

# add the task to the taskManager
QgsApplication.taskManager().addTask(task)
