from functools import partial
from qgis.core import (QgsTaskManager, QgsMessageLog,
                       QgsProcessingAlgRunnerTask, QgsApplication,
                       QgsProcessingContext, QgsProcessingFeedback,
                       QgsProject)

MESSAGE_CATEGORY = 'AlgRunnerTask'

def task_finished(context, successful, results):
    if not successful:
        QgsMessageLog.logMessage('Task finished unsucessfully',
                                 MESSAGE_CATEGORY, Qgis.Warning)
    output_layer = context.getMapLayer(results['OUTPUT'])
    # because getMapLayer doesn't transfer ownership, the layer will
    # be deleted when context goes out of scope and you'll get a
    # crash.
    # takeMapLayer transfers ownership so it's then safe to add it
    # to the project and give the project ownership.
    if output_layer and output_layer.isValid():
            QgsProject.instance().addMapLayer(
                 context.takeResultLayer(output_layer.id()))

layers = iface.mapCanvas().layers() # get the layers shown on the canvas
for layer in layers:
    if(layer.name() == '2. from-a-processing-algorithm'):
        input_layer = layer
        break

# arguments for the QgsProcessingAlgRunnerTask
alg = QgsApplication.processingRegistry().algorithmById(
                                          'native:fieldcalculator')
# processing.algorithmHelp('native:fieldcalculator') 

context = QgsProcessingContext()
feedback = QgsProcessingFeedback()
params = {
    'INPUT' : input_layer,
    'FIELD_NAME': 'adr_label',
    'FIELD_TYPE': 2,
    'FIELD_LENGTH': 254,
    'FIELD_PRECISION': 0,
    'FORMULA': "if(length(numero) = 0, '', numero || ', ') || rue || '\nL-' || code_posta || ' ' || localite",
    'OUTPUT': 'memory:{}'.format(input_layer.name() + '_output')
}
task = QgsProcessingAlgRunnerTask(alg, params, context, feedback)
task.executed.connect(partial(task_finished, context))
QgsApplication.taskManager().addTask(task)